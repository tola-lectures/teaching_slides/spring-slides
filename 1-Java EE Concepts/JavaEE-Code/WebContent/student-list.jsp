<%@page import="com.kshrd.app.model.dto.StudentDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student List</title>
</head>
<body>

<h2>Student List</h2>

<a href="${pageContext.request.contextPath}/student-add-form.act">Add Student</a>

<%
	ArrayList<StudentDTO> students = (ArrayList)request.getAttribute("students");
	out.print("size : " + students.size());
%>

<table border="1">
	<thead>
		<tr>	
			<th>ID</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Major</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<%for(int i=0;i< students.size();i++){ %>
		<tr>
			<td><%= students.get(i).getId() %></td>
			<td><%= students.get(i).getName() %></td>
			<td><%= students.get(i).getGender() %></td>
			<td><%= students.get(i).getMajor() %></td>
			<td>
				<a href="${pageContext.request.contextPath}/student-update-form.act?id=<%= students.get(i).getId() %>"">Update</a> 
				|
				<a href="${pageContext.request.contextPath}/student-delete.act?id=<%= students.get(i).getId() %>"> Delete </a></td>
		</tr>
		<% } %>
	</tbody>
</table>

</body>
</html>