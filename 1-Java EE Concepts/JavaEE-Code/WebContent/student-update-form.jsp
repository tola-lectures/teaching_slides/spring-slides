<%@page import="com.kshrd.app.model.dto.StudentDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student Add Form</title>
</head>
<body>

<h2>Add Student</h2>
<a href="${pageContext.request.contextPath}/student-list.act">Back</a>

<% 
	StudentDTO student = (StudentDTO)request.getAttribute("student");
%>

<form action="${pageContext.request.contextPath}/student-update-process.act">

	<table>
		<tr>
			<td><label>ID : </label></td>
			<td><input type="text" readonly="readonly" name="id" value="<%= student.getId() %>"> </td>
		</tr>
		<tr>
			<td><label>Name : </label></td>
			<td><input type="text" name="name" value="<%= student.getName() %>"> </td>
		</tr>
		<tr>
			<td><label>Gender : </label></td>
			<td>
				<select name="gender">
					<% if (student.getGender().equals("M")) {%>
							<option value="M" selected="selected">Male</option>
							<option value="F">Female</option>
					<% } else{  %>
							<option value="M">Male</option>
							<option value="F"  selected="selected">Female</option>
					<% } %>
					
					
				</select>
			</td>
		</tr>
		<tr>
			<td><label>Major : </label></td>
			<td><input type="text" name="major" value="<%= student.getMajor() %>"> </td>
		</tr>
	</table>
	
	<input type="submit" value="Update"/>

</form>


</body>
</html>