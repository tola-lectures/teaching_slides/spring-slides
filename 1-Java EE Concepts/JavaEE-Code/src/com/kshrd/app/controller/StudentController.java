package com.kshrd.app.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kshrd.app.controller.student.ActionStudentAdd;
import com.kshrd.app.controller.student.ActionStudentDelete;
import com.kshrd.app.controller.student.ActionStudentGetByID;
import com.kshrd.app.controller.student.ActionStudentList;
import com.kshrd.app.controller.student.ActionStudentUpdateByID;

/**
 * Servlet implementation class StudentController
 */
@WebServlet("*.act")
public class StudentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request , response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request , response);
	}
	
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String requestURI = request.getRequestURI();
		String contextPath = request.getContextPath();
		String command = request.getRequestURI().substring(contextPath.length());
		Action action = null;
		ActionForward forward = null;
		
		System.out.println("requestURI : "+ requestURI);
		System.out.println("contextPath : "+ contextPath);
		System.out.println("command : "+ command);

		switch (command) {
		
		/*  // Return to view without data
		case "/student-list.act":
			System.out.println("/student-list.act");
			forward = new ActionForward();
			forward.setPath("student-list.jsp");
			break;
		*/
		
		case "/student-list.act":
			System.out.println("/student-list.act");
			action = new ActionStudentList();
			forward = action.execute(request, response);
		break;
		
		case "/student-delete.act" :
			System.out.println("student-delete.act");
			action = new ActionStudentDelete();
			forward = action.execute(request, response);
		break;
		
		
		
		
		// Show Student Form 
		case "/student-add-form.act":
			System.out.println("/student-add-form.act");
			forward = new ActionForward();
			forward.setPath("student-add-form.jsp");
			break;
		
		// 	Add Student to Database
		case "/student-add-process.act" :
			action = new ActionStudentAdd();
			forward = action.execute(request, response);
		break;
			
		/*
		case "/student-update-form.act":
			System.out.println("/student-update-form.act");
			forward = new ActionForward();
			forward.setPath("student-update-form.jsp");
			break;
		*/
		
		case "/student-update-form.act":
			System.out.println("/student-update-form.act");
			action = new ActionStudentGetByID();
			forward = action.execute(request, response);
		break;
		
		case "/student-update-process.act" :
			action = new ActionStudentUpdateByID();
			forward = action.execute(request, response);
			break;
		
		}
		
		
		if(forward != null){
			if(forward.isRedirect()){
				response.sendRedirect(forward.getPath());
			}else{
				RequestDispatcher dispatcher = request.getRequestDispatcher(forward.getPath());
				dispatcher.forward(request, response);
			}
		}
		
	}
	
	

}
