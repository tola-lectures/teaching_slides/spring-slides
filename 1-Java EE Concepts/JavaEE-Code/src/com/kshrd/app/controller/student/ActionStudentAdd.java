package com.kshrd.app.controller.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kshrd.app.controller.Action;
import com.kshrd.app.controller.ActionForward;
import com.kshrd.app.model.dao.StudentDAO;
import com.kshrd.app.model.dto.StudentDTO;

public class ActionStudentAdd implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) {
		
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String major = request.getParameter("major");
		
		System.out.println(name + " " + gender + " " + major);
		
		StudentDTO student = new StudentDTO();
		student.setName(name);
		student.setMajor(major);
		student.setGender(gender);
		
		if(new StudentDAO().addStudent(student)){
			System.out.println("Student Name " + name +" has been inserted!" );
		}else{
			System.out.println("Student Name " + name +" has not been inserted!" );
		}
		
		ActionForward forward = new ActionForward();
		forward.setPath("student-list.act");
		forward.setRedirect(true);
		return forward;
	}

}
