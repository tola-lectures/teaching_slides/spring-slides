package com.kshrd.app.controller.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.kshrd.app.controller.Action;
import com.kshrd.app.controller.ActionForward;
import com.kshrd.app.model.dao.StudentDAO;

public class ActionStudentDelete implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) {
		
		if(new StudentDAO().deleteStudent(Integer.parseInt(request.getParameter("id")))){
			System.out.println("Student has been deleted!");
		}else{
			System.out.println("Student has not been deleted!");
		}
			
		
		
		ActionForward forward = new ActionForward();
		forward.setRedirect(true);
		forward.setPath("student-list.act");
		return forward;
	}

}
