package com.kshrd.app.controller.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kshrd.app.controller.Action;
import com.kshrd.app.controller.ActionForward;
import com.kshrd.app.model.dao.StudentDAO;

public class ActionStudentGetByID implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) {
		
		
		int id = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("student", new StudentDAO().getStudentByID(id));
		
		ActionForward forward = new ActionForward();
		forward.setPath("student-update-form.jsp");
		return forward;
	}

}
