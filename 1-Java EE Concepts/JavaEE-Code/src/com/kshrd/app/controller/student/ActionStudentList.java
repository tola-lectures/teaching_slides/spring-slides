package com.kshrd.app.controller.student;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kshrd.app.controller.Action;
import com.kshrd.app.controller.ActionForward;
import com.kshrd.app.model.dao.StudentDAO;
import com.kshrd.app.model.dto.StudentDTO;

public class ActionStudentList implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) {
		
		//StudentDAO studentDAO = new StudentDAO();
		
		//ArrayList<StudentDTO> students = studentDAO.listStudent();
		
		request.setAttribute("students", new StudentDAO().listStudent());
		ActionForward forward = new ActionForward();
		forward.setPath("student-list.jsp");
		return forward;
	}

}
