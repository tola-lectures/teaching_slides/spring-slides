package com.kshrd.app.controller.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kshrd.app.controller.Action;
import com.kshrd.app.controller.ActionForward;
import com.kshrd.app.model.dao.StudentDAO;
import com.kshrd.app.model.dto.StudentDTO;

public class ActionStudentUpdateByID implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) {

		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		String major = request.getParameter("major");
		
		StudentDTO dto = new StudentDTO();
		dto.setId(id);
		dto.setName(name);
		dto.setMajor(major);
		dto.setGender(gender);
		
		if(new StudentDAO().updateStudent(dto)){
			System.out.println("Student name "+ name +" has been updated");
		}else{
			System.out.println("Student name "+ name +" has not been updated");
		}
		
		ActionForward forward = new ActionForward();
		forward.setPath("student-list.act");
		forward.setRedirect(true);
		return forward;
	}

}
