package com.kshrd.app.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	public Connection getConnection() throws SQLException, ClassNotFoundException{
		Class.forName("org.postgresql.Driver");
		Connection cnn = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/STUDENT_DB", // URL
				"postgres", // USER
				"123456"); // Password
		return cnn;
	}
	
	

}
