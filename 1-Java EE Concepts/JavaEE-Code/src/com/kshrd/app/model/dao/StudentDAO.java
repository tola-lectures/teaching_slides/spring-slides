package com.kshrd.app.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.kshrd.app.model.dto.StudentDTO;

public class StudentDAO {
	
	public ArrayList<StudentDTO> listStudent(){
		ArrayList<StudentDTO> students = new ArrayList<StudentDTO>();
		StudentDTO student = null;
		try{
			Connection cnn = new DBConnection().getConnection();
			ResultSet rs = null;
			String sql ="SELECT id , name , gender , major FROM students ORDER BY id DESC";
			PreparedStatement ps = cnn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				student = new StudentDTO();
				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setGender(rs.getString("gender"));
				student.setMajor(rs.getString("major"));
				students.add(student);
				System.out.println(rs.getString("name"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return students;
	}
	
	
	public StudentDTO getStudentByID(int id){
		StudentDTO student = null;
		try{
			Connection cnn = new DBConnection().getConnection();
			ResultSet rs = null;
			String sql ="SELECT id , name , gender , major FROM students WHERE id=?";
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				student = new StudentDTO();
				student.setId(rs.getInt("id"));
				student.setName(rs.getString("name"));
				student.setGender(rs.getString("gender"));
				student.setMajor(rs.getString("major"));
				System.out.println(rs.getString("name"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return student;
	}
	
	public boolean addStudent(StudentDTO student){
		try{
			Connection cnn = new DBConnection().getConnection();
			String sql = "INSERT INTO students(name, gender, major) values (?,?,?);";
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setString(2, student.getGender());
			ps.setString(3, student.getMajor());
			if(ps.executeUpdate() > 0) return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean updateStudent(StudentDTO student){
		try{
			Connection cnn = new DBConnection().getConnection();
			String sql = "UPDATE students SET name=?, gender=?, major=? WHERE id=?";
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setString(1, student.getName());
			ps.setString(2, student.getGender());
			ps.setString(3, student.getMajor());
			ps.setInt(4, student.getId());
			if(ps.executeUpdate() > 0) return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteStudent(int id){
		try{
			Connection cnn = new DBConnection().getConnection();
			String sql = "DELETE FROM students WHERE id=?";
			PreparedStatement ps = cnn.prepareStatement(sql);
			ps.setInt(1, id);
			if(ps.executeUpdate() > 0) return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	
//	public static void main(String[] args) {
//		StudentDTO s = new StudentDTO();
//		s.setName("Pirang");
//		s.setMajor("Khmer");
//		s.setGender("M");
//		new StudentDAO().addStudent(s);
//	}

}
